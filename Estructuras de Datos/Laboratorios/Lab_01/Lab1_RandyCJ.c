/*  Laboratorio 1. Estructuras de Datos. Grupo 20.
	Estudiante: Randy Conejo Juárez. 2019066448 */


#include <stdio.h>
#include <string.h>

struct estudiante{
    char nombre[15];
    int carnet;
};

int validar_carnet(struct estudiante alumnos[10]);

int main(){
    // Función principal, en ella se obtienen los datos de los 10 estudiantes.
    
    struct estudiante alumnos[10];
    int posicion_solicitada;
    int carnet_ingresado;
    int eleccion = 1;

    // Bucle para preguntar 10 veces por datos.
    for (int i=0;i<10;i++){
        printf("Ingrese nombre %d \n", i);
        scanf("%s", &alumnos[i].nombre);
        
        printf("Ingrese carnet %d \n", i);
        scanf("%d", &alumnos[i].carnet);
    }
    
    // Bucle que permite decidir si seguir o no ejecutando el programa.
    while (eleccion != 0){
        eleccion = validar_carnet(alumnos);
    }
    return 0;
}

int solicita_posicion(){
    // Función encargada de solicitar al usuario la posición de carnet que desea validar.
    
    int posicion_solicitada = 0;
    
    printf("¿Que posición de carnet desea validar? \n");
    scanf("%d", &posicion_solicitada);
    
    if (posicion_solicitada>9){
        printf("\n");
        printf("Favor ingresar un número entre 0 y 9 \n");
        solicita_posicion();
    }
    else if (posicion_solicitada<0){
        printf("\n");
        printf("Favor ingresar un número entre 0 y 9 \n");
        solicita_posicion();
    }
    else {return posicion_solicitada;};
}

int solicita_carnet(pos_solicitada){
    // Función encargada de preguntar por el carnet del estudiante seleccionado.
    
    int carnet_ingresado;
    
    printf("Ingrese el carnet del estudiante en la posicion %d \n", pos_solicitada);
    scanf("%d", &carnet_ingresado);
    printf("\n");
    
    return carnet_ingresado;
}

int validar_carnet(struct estudiante alumnos[3]){
    /* Función encargada de validar el carnet ingresado con el guardado en el struct
     * E: un arreglo de struct con los datos de los estudiantes
     * S: la función retorna 1 o 0 para determinar si el usuario quiere seguir validando carnets 
    */
    
    int posicion_solicitada = solicita_posicion();
    int carnet_ingresado = solicita_carnet(posicion_solicitada);
    int eleccion;
    
    if (carnet_ingresado != alumnos[posicion_solicitada].carnet){
        printf("El carnet ingresado no corresponde con la posición %d \n \n", posicion_solicitada);
    }
    else {printf("El carnet ingresado es correcto \n \n");};
    
    printf("¿Desea seguir validando números de carnet?\n");
    printf("Si digita no, tendrá que volver a escribir los datos de los 10 estudiantes \n");
    printf("\n");
    printf("DIGITE 0 (CERO) PARA NO, DIGITE CUALQUIER OTRO NÚMERO PARA SÍ \n");
    scanf("%d", &eleccion);
    return eleccion;
}
