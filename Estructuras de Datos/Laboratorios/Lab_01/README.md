**Laboratorio #1.**

Escriba un programa en C en el que se pueda solicitar los datos de 10 estudiantes para después almacenarlos en un arreglo de tamaño 10 de tipo struct estudiante.
El programa debe preguntar por el nombre y carnet de 10 estudiantes, después de ello el programa debe seguir el siguiente comportamiento.


PC$ ¿Que posición desea validar?

	> 2


PC$ ¿Cuál es el carnet del estudiante en la posición 2?

	> 201925317



*Si el carnet está correcto*

	PC$ El carnet ingresado es correcto.


*Si el carnet está erroneo*

	PC$ El carnet ingresado no corresponde con la posición 2.
