/*  Laboratorio 2. Estructuras de Datos. Grupo 20.
    Estudiante: Randy Conejo Juárez. 2019066448
    Tecnológico de Costa Rica, Sede Alajuela
    Prof. Samanta Ramijan Carmiol 
*/

/* La declaración de la lista y la función agrega_datos fueron adaptados de los siguientes videos
 * https://www.youtube.com/watch?v=WxoGvBzWuGs; 
 * https://www.youtube.com/watch?v=Bs5tT29w2t4; 
 * https://www.youtube.com/watch?v=5KiKzWn_jMM;
 */


// Librerias necesarias
#include <stdio.h>
#include <stdlib.h>

// Se declara la estructura y la lista a usar, se le coloca nombre para referenciarse a ella más fácil
typedef struct nodo{
    char *nombre;
    int *carnet;
    struct nodo *sig;
}Estudiante;

Estudiante *lista_estudiantes(Estudiante *Lista){
    // Función que al ser llamada empieza la lista dándole un valor nulo.
    Lista = NULL;
    return Lista;
}


Estudiante *agrega_datos(Estudiante *Lista, char *nombre, int *carnet){
    /* Función que agrega los datos de los estudiantes a la lista, retorna la lista con los elementos ya agregados
     * Código adaptado de los siguientes videos: https://www.youtube.com/watch?v=Bs5tT29w2t4; 
     * https://www.youtube.com/watch?v=5KiKzWn_jMM
     */
    Estudiante *nuevo_estudiante, *temp;
    
    nuevo_estudiante = (Estudiante*)malloc(sizeof(Estudiante));
    nuevo_estudiante->nombre = nombre;
    nuevo_estudiante->carnet = carnet;
    nuevo_estudiante->sig = NULL;
    
    if (Lista == NULL){
        Lista = nuevo_estudiante;
    }
    else{ // En caso de que la lista tenga elementos
        temp = Lista;
        while (temp->sig != NULL){
            temp = temp->sig;
        }
        temp->sig = nuevo_estudiante;
    }

    return Lista;
}

int main(){
    // Funcíon que controla el flujo del programa, solicita los datos de los estudiantes y llama a otra función.
    
    Estudiante *Lista = lista_estudiantes(Lista);
    int cantidad;
    char nombre_estudiante[15];
    int carnet;
    int eleccion = 1;
    
    printf("Ingrese la cantidad de estudiantes que desea almacenar\n");
    scanf("%d", &cantidad);
    
    for (int contador = 0; contador < cantidad; contador++){
        printf("Ingrese nombre del estudiante en la posición %d \n", contador);
        scanf("%s", &nombre_estudiante);
        printf("Ingrese carnet del estudiante en la posición %d \n", contador);
        scanf("%d", &carnet);
        Lista = agrega_datos(Lista, nombre_estudiante, carnet);
    }
    
    while (eleccion != 0){
        eleccion = validar_carnet(Lista, cantidad);
    }
    
    return 0;
}

int validar_carnet(Estudiante *Lista, int cantidad){
    /* Función encargada de validar el carnet ingresado con el guardado en el la lista
     * E: la lista con los datos de los estudiantes, la cantidad de estudiantes que hay almacenados
     * S: la función retorna 1 o 0 para determinar si el usuario quiere seguir validando carnets 
    */
    
    // Se obtienen los datos requeridos del usuario para verificar el carnet
    int posicion_solicitada = solicita_posicion(cantidad -1);
    int carnet_ingresado = solicita_carnet(posicion_solicitada);
    
    int eleccion;
    int contador = 0;
    
    // Este ciclo se encarga de posicionarnos sobre la posición que solicitó el usuario
    while (posicion_solicitada != contador){
        Lista = Lista->sig;
        contador += 1;
    }
    if (carnet_ingresado != Lista->carnet){
        printf("El carnet ingresado no corresponde con la posición %d \n \n", posicion_solicitada);
    }
    else {
        printf("El carnet ingresado es correcto \n \n");
    };
    
    printf("¿Desea seguir validando números de carnet?\n");
    printf("DIGITE 0 (CERO) PARA NO, DIGITE CUALQUIER OTRO NÚMERO PARA SÍ \n");
    scanf("%d", &eleccion);
    return eleccion;
}



int solicita_posicion(limite){
    // Función encargada de solicitar al usuario la posición de carnet que desea validar.
    
    int posicion_solicitada = 0;
    
    printf("¿Que posición de carnet desea validar? \n");
    scanf("%d", &posicion_solicitada);
    
    if (posicion_solicitada>limite){
        printf("\n");
        printf("Favor ingresar un número entre 0 y %d \n", limite);
        solicita_posicion(limite);
    }
    else if (posicion_solicitada<0){
        printf("\n");
        printf("Favor ingresar un número entre 0 y %d \n", limite);
        solicita_posicion(limite);
    }
    else {return posicion_solicitada;};
}

int solicita_carnet(pos_solicitada){
    // Función encargada de preguntar por el carnet del estudiante seleccionado.
    
    int carnet_ingresado;
    
    printf("Ingrese el carnet del estudiante en la posicion %d \n", pos_solicitada);
    scanf("%d", &carnet_ingresado);
    printf("\n");
    
    return carnet_ingresado;
}
