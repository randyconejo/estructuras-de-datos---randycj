/*  Laboratorio 3. Estructuras de Datos. Grupo 20.
    Estudiante: Randy Conejo Juárez. 2019066448
    Tecnológico de Costa Rica, Sede Alajuela
    Prof. Samanta Ramijan Carmiol 
*/


// Librerias necesarias
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


// Se declara la estructura y la lista a usar, se le coloca el nombre estudiante para referenciarse a ella más fácil

typedef struct nodo{
    char *nombre;
    int *carnet;
    struct nodo *sig;
}Estudiante;


Estudiante *agrega_datos(Estudiante *Lista, int opcion){
    /* Función que agrega los datos de los estudiantes a la lista
     * E: struct de tipo Estudiante, número entero que indica si agrega un estudiante al principio o al final de la lista
     * S: retorna un struct Estudiante con los datos de un nuevo estudiante
     */
     
    //Declaración de variables que serán utilizadas
    Estudiante *nuevo_estudiante, *temp;
    
    
    switch (opcion){
        
        case 0://Agrega estudiante al principio de la lista
        
            nuevo_estudiante = (Estudiante*)malloc(sizeof(Estudiante));//Reserva el espacio para agregar al estudiante
            printf("Ingrese nombre\n");
            scanf("%s", &nuevo_estudiante->nombre);
            printf("Ingrese carnet\n");
            scanf("%d", &nuevo_estudiante->carnet);
            nuevo_estudiante->sig = Lista;
            printf("Estudiante agregado exitosamente\n\n");
            return nuevo_estudiante;
            

        case 1://Agrega estudiante al final de la lista
            
            nuevo_estudiante = (Estudiante*)malloc(sizeof(Estudiante));//Reserva el espacio para agregar al estudiante
            printf("Ingrese nombre\n");
            scanf("%s", &nuevo_estudiante->nombre);
            printf("Ingrese carnet\n");
            scanf("%d", &nuevo_estudiante->carnet);
            nuevo_estudiante->sig = NULL;
            
            if (Lista == NULL){//Si la lista no tiene ningún estudiante, se le asigna el nuevo estudiante en el primer lugar
                Lista = nuevo_estudiante;
            }
            else{ // En caso de que la lista ya tenga estudiantes, la recorre hasta el final y agrega el estudiante nuevo al final
                temp = Lista;
                while (temp->sig != NULL){
                    temp = temp->sig;
                }
                temp->sig = nuevo_estudiante;
            }
            return Lista;
        
    }
}


void validar_carnet(Estudiante *Lista, int cantidad){
    /* Función encargada de validar el carnet ingresado con el guardado en el la lista
     * E: la lista con los datos de los estudiantes, la cantidad de estudiantes que hay almacenados
    */
    
    if (cantidad == 0){//Se asegura que exista al menos un estudiante en la lista
        printf("No hay ningún estudiante guardado\n\n");
        return Lista;
    }
    
    // Se obtienen los datos requeridos del usuario para verificar el carnet
    int posicion_solicitada = solicita_posicion(cantidad -1);
    int carnet_ingresado = solicita_carnet(posicion_solicitada);
    
    int eleccion;
    int contador = 0;
    
    // Este ciclo se encarga de iterar la lista para llegar a la posición que solicitó el usuario
    while (posicion_solicitada != contador){
        Lista = Lista->sig;
        contador += 1;
    }
    if (carnet_ingresado != Lista->carnet){
        printf("El carnet ingresado no corresponde con la posición %d \n \n", posicion_solicitada);
    }
    else {
        printf("El carnet ingresado es correcto \n \n");
    }
    
}



int solicita_posicion(limite){
    /* Función encargada de solicitar al usuario la posición de carnet que desea validar.
     * Tiene de armunento limite, el cual sirve para restringir al usuario y que no pueda ingresar un número mayor a la cantidad de estudiantes guardados
     */
    
    
    int posicion_solicitada = 0;
    
    printf("¿Que posición de carnet desea validar? \n");
    scanf("%d", &posicion_solicitada);
    
    if (posicion_solicitada>limite){
        printf("\n");
        printf("Favor ingresar un número entre 0 y %d \n", limite);
        solicita_posicion(limite);
    }
    else if (posicion_solicitada<0){
        printf("\n");
        printf("Favor ingresar un número entre 0 y %d \n", limite);
        solicita_posicion(limite);
    }
    else {return posicion_solicitada;};
}

int solicita_carnet(pos_solicitada){
    // Función encargada de preguntar por el carnet del estudiante seleccionado.
    
    int carnet_ingresado;
    
    printf("Ingrese el carnet del estudiante en la posicion %d \n", pos_solicitada);
    scanf("%d", &carnet_ingresado);
    printf("\n");
    
    return carnet_ingresado;
}

Estudiante *elimina_estudiante(Estudiante *Lista, int cantidad){
    
    Estudiante *tempA, *tempB;//Se declaran dos variables temporales
    int posicion;
    tempA = Lista;//Se le asigna el valor del primer elemento de la lista a la variable temporal

    if (cantidad == 0){//Si no hay ningún estudiante agregado a la lista
        printf("No hay ningún estudiante para eliminar\n\n");
        return Lista;
    }

    printf("Ingrese la posicion del estudiante que desea eliminar: ");
    scanf("%d", &posicion);

    while (posicion > cantidad -1){
      printf("\nNo existe estudiante en la posicion indicada");
      printf("\nIngrese la posicion del estudiante que desea eliminar: ");
      scanf("%d", &posicion);
    }

    if (posicion == 0){//Caso en que se elimina el primero de la lista
        Lista = Lista->sig;//Brincamos al segundo estudiante
        free(tempA);//Liberamos el espacio en memoria del primer estudiante
        printf("\nEstudiante eliminado exitosamente\n\n");
        return Lista;
    }
    
    else if(posicion == cantidad -1){//Caso en el que se elimina al último de la lista
        for (int i=0; i<cantidad-1; i++){//Se recorre la lista hasta llegar al penúltimo estudiante
            tempA = tempA->sig;
        }
        free(tempA->sig);//Eliminamos el espacio en memoria del siguiente estudiante, el cual es el último
        printf("\nEstudiante eliminado exitosamente\n\n");
        return Lista;
    }
    
    else{//Caso en el que se elimina un estudiante en cualquier otra posicion
        
        for (int i=0; i<posicion-1;i++){//Recorre la lista hasta llegar al estudiante que está antes del que se desea eliminar
            tempA = tempA->sig;
        }
        
        tempB = tempA;//Asignamos el estudiante que está antes del eliminado a una variable auxiliar
        tempA=tempA->sig;//Nos colocamos en el estudiante a eliminar
        tempB->sig = NULL;//Borramos los datos del estudiante que vamos a eliminar
        tempB->sig = tempA->sig;//El estudiante anterior del eliminado apunta al siguiente del eliminado
        free(tempA);//Liberamos es espacio del estudiante eliminado
        printf("\nEstudiante eliminado exitosamente\n\n");
        return Lista;
    }
}

int main(){
    // Funcíon que controla el flujo del programa
    
    Estudiante *Lista = NULL;
    int cantidad;//cantidad de estudiantes actuales en la lista
    int eleccion = 1;
    int opcion;
    Estudiante *temp;
    
    printf("Ingrese la cantidad inicial de estudiantes que desea ingresar\n");
    scanf("%d", &cantidad);
    
    for (int contador = 0; contador < cantidad; contador++){
        Lista = agrega_datos(Lista, 1);
    }
    printf("\n");

    while (eleccion = 1){//Implementación de un menú principal y de la captura de acciones
        
        printf("Menú principal\n");
        printf("1. Ingresar estudiante al principio de la Lista\n");
        printf("2. Ingresar estudiante al final de la lista\n");
        printf("3. Verificar carnet de un estudiante\n");
        printf("4. Eliminar a estudiante de la lista\n");
        printf("5. Imprimir lista de estudiantes\n");
        printf("6. Salir\n");
        printf("\n");
        printf("Ingrese su selección: ");
        scanf("%d", &opcion);
        printf("\n");
    
        if (opcion!=6){
            
            switch(opcion){
                
                case 1://Agrega un estudiante al principio de la lista
                    Lista = agrega_datos(Lista, 0);
                    cantidad += 1;
                    break;
                
                case 2://Agrega un estudiante al final de la lista
                    Lista = agrega_datos(Lista, 1);
                    cantidad += 1;
                    printf("Estudiante agregado exitosamente\n\n");
                    break;
                    
                case 3://Pregunta por el carnet de un estudiante
                    validar_carnet(Lista, cantidad);
                    break;
                    
                case 4://Elimina un estudiante de la lista
                    Lista = elimina_estudiante(Lista, cantidad);
                    if (cantidad != 0){//Para que cantidad no se quede en valores negativos
                        cantidad -=1;
                    }
                    break;
                
                case 5://Imprime la lista de estudiantes actuales
                    if (cantidad == 0){
                        printf("No hay estudiantes guardados\n\n");
                    }
                    else{
                        temp = Lista;
                        printf("Nombre  Carnet\n");
                        for (int i = 0; i<cantidad; i++){
                            printf("%s  %d \n", &Lista->nombre, Lista->carnet);
                            Lista = Lista->sig;
                        }
                        Lista = temp;
                        printf("\n");
                }
            }
        }
        else{return 0;}//Se sale del programa
    }
}
