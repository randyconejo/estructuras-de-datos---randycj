**Laboratorio #3.**

Utilice como base el código del laboratorio #2 y extiéndalo para que pueda realizar las siguientes acciones


1. Agregar un nuevo estudiante al inicio de la lista.


2. Borrar un estudiante a partir de su posición en la lista. Recuerde liberar el espacio en memoria reservado, una vez que no lo esté utilizando.


3. Implemente un menú con las siguientes opciones: 

	
	(I)insertar estudiante al final de la lista.


	(II)insertar estudiante al principio de la lista.


	(III)verificar el carnet de un estudiante en una posición dada.


	(IV)eliminar de la lista a un estudiante en una posicion dada


	(V)salir del programa
