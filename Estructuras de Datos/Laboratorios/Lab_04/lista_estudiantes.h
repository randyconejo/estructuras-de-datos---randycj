/**********************************************************************
    Instituto Tecnológico de Costa Rica
    Estructuras de Datos IC-2001
    II Semestre 2019
    Profesora: Samanta Ramijan Carmiol
    Ejemplos Prácticos: Lista de Estudiantes
    Estudiante: Randy Conejo Juárez
**********************************************************************/
//Definición de Macros
#define LONGITUD_MAXIMA_NOMBRE 50 
#define LONGITUD_MAXIMA_CARNET 12

//Definición de Estructuras
typedef struct nodo_estudiante
{
	int carnet;
	char *nombre;

	struct nodo_estudiante *ref_siguiente;
}nodo_estudiante;

typedef struct lista_estudiantes
{
	nodo_estudiante *ref_inicio;
	int cantidad;
}lista_estudiantes;

//Definición de Funciones
/*-----------------------------------------------------------------------
	crear_nodo
	Entradas: No recibe parámetros
	Salidas: Retorna un puntero de tipo nodo_estudiante al nodo creado
	Funcionamiento: 
		- Solicita al usuario ingresar Nombre y Carnet.
		- Crea un puntero de tipo nodo_estudiante.
		- Le asigna al nodo el nombre y carnet ingresados.
		- El nodo apunta a NULL.
		- Retorna el puntero al nuevo nodo.
-----------------------------------------------------------------------*/
nodo_estudiante* crear_nodo();
/*-----------------------------------------------------------------------
	inicializar_lista
	Entradas: No recibe parametros.
	Salidas: No retorna ningún valor.
	Funcionamiento: 
        - Inicializa la lista
        - Le asigna espacio a la lista.
        - Establece ref_inicio como NULL.
        - Establece la cantidad de estudiantes en 0.
-----------------------------------------------------------------------*/
void inicializar_lista();
/*-----------------------------------------------------------------------
	insertar_inicio
	Entradas: Recibe un puntero de tipo nodo_estudiante.
	Salidas: No retorna ningún valor.
	Funcionamiento: 
        - Si la lista no se ha inicializado, la inicializa.
        - Si la lista está vacia, ref_lista apunta al argumento en ref_inicio y el elemento nuevo apunta a si mismo en ref_siguiente.
        - Si la lista no está vacia, el nuevo elemento apunta al que estaba de primero, y el último ahora apunta al nuevo.
-----------------------------------------------------------------------*/
void insertar_inicio(nodo_estudiante* nuevo);
/*-----------------------------------------------------------------------
	insertar_final
	Entradas: Recibe un puntero de tipo nodo_estudiante.
	Salidas: No retorna ningun valor.
	Funcionamiento: 
        - Si la lista no se ha inicializado, la inicializa.
        - Si la lista está vacia, ref_lista apunta al argumento en ref_inicio y el elemento nuevo apunta a si mismo en ref_siguiente.
        - Si la lista no está vacia, el último elemento apuntará al nuevo, y el nuevo elemento apuntará al primero de la lista.
-----------------------------------------------------------------------*/
void insertar_final(nodo_estudiante* nuevo);
/*-----------------------------------------------------------------------
	borrar_por_indice
	Entradas: Un número tipo entero.
	Salidas: No retorna ningún valor.
	Funcionamiento: 
        - Si la lista está vacia imprime un mensaje de que no hay estudiantes.
        - Si se quiere eliminar el primer elemento, ref_lista apuntaría al segundo elemento y por medio de un while se llega hasta el 
          último elemento para que este apunte ahora al segundo elemento y se libera el espacio del primero.
        - Si se quiere eliminar el último elemento, se llega hasta el penúltimo por medio de un while, se utiliza una nueva variable para poder llegar al
          último elemento y se libera el espacio, después el penúltimo apuntaría al primer elemento de la lista.
        - Si no es ni el primero ni último, se recorre la lista hasta llegar al elemento anterior a eliminar, el penúltimo apuntaría al que sigue después del último
          y se libera el espacio del elemento que se eliminó.
-----------------------------------------------------------------------*/
void borrar_por_indice(int indice);
 /*-----------------------------------------------------------------------
	buscar_por_indice
	Entradas: Un número tipo entero.
	Salidas: Retorna un puntero hacia el elemento a buscar
	Funcionamiento: 
        - Si la lista no tiene estudiantes lo imprime.
        - Recorre la lista hasta llegar a la posición solicitada por el usuario.
        - Retorna el puntero de la posición.
-----------------------------------------------------------------------*/
nodo_estudiante* buscar_por_indice(int indice);

 /*-----------------------------------------------------------------------
	validar_carnets
	Entradas: Dos números tipo entero, 
	Salidas: No retorna ningún valor
	Funcionamiento: 
        - Compara el carnet ingresado por el usuario con el guardado en la lista.
        - Si es igual, imprime que el carnet es correcto.
        - Si no, imprime que es incorrecto.
-----------------------------------------------------------------------*/
void validar_carnets(int carnet_almacenado, int carnet_ingresado);
 /*-----------------------------------------------------------------------
	menu
	Entradas: No recibe parámetros.
	Salidas: No retorna ningún valor.
	Funcionamiento: 
        - Primero imprime la lista de estudiantes.
        - Imprime el menú para el usuario y solicita un número para realizar una acción.
        - Con el 0 termina el programa.
        - Con el 1 llama a la función de crear_nodo y luego a la de insertar_final.
        - Con el 2 llama a crear_nodo y después a insertar_inicio.
        - Con el 3 solicita al usuario una posición para validar, llama a buscar_por_indice para traer al elemento solicitado por 
          el usuario y luego llama a validar_carnets.
        - Con el 4 solicita al usuario una posición para eliminar, luego llama a borrar_por_indice.
        - Termina el ciclio y se vuelve a imprimir el menú.
-----------------------------------------------------------------------*/
void menu();
 /*-----------------------------------------------------------------------
	main
	Entradas: No recibe parámetros.
	Salidas: No retorna ningún valor.
	Funcionamiento:
        - Llama a la función menú.
-----------------------------------------------------------------------*/
int main();
 /*-----------------------------------------------------------------------
	get_user_input
	Entradas: Recibe el máximo de caracteres que puede ingresar el usuario.
	Salidas: Retorna lo que escribió el usuario.
	Funcionamiento: 
        - Guarda en una variable lo que escribió el usuario, utiliza el argumento para determinar el máximo de caracteres que puede 
          ingresar el usuario.
-----------------------------------------------------------------------*/
char* get_user_input(size_t max_size);
 /*-----------------------------------------------------------------------
	get_user_numerical_input
	Entradas: Recibe el máximo de caracteres que puede ingresar el usuario.
	Salidas: Retorna lo que escribió el usuario.
	Funcionamiento: 
        - Usa la función atoi() para llamar a la función get_user_input y convertir el string que recibe en un entero.
        - Retorna el número capturado.
-----------------------------------------------------------------------*/
int get_user_numerical_input(size_t max_size);

