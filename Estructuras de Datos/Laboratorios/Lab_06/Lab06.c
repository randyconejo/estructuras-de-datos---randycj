#include <stdio.h>
#include <stdlib.h>
#include "Lab06.h"


ArbolBinario *refArbol = NULL;


void main() {
    refArbol = malloc(sizeof(ArbolBinario));
    crear_arbol();
    imprimir_arbol();
    printf("Resultado de hacer un recorrido postorden\n");
    recorrer_arbol(refArbol->raiz);
    printf("\n");
    return;
}

void crear_arbol(){
    /* Esta función es muy específica y crea siempre el mismo árbol con los mismos valores, se podría implementar
     * una nueva función de insertar pero para lo solicitado en este laboratorio con esto es suficiente
     */
    
    Nodo *temporal = NULL;
    temporal = malloc(sizeof(Nodo));
    Nodo *temporalIzquierdo = NULL;
    temporalIzquierdo = malloc(sizeof(Nodo));
    Nodo *temporalDerecho = NULL;
    temporalDerecho = malloc(sizeof(Nodo));
    
    temporal = nueva_raiz(8);
    refArbol->raiz = temporal;//Crea la raíz
    
    //Sub-árbol izquierdo de la raíz
    temporal->HijoIzquierdo = nueva_raiz(5);//Crea el nodo 5 (Hijo izquierdo de la raiz)
    temporalIzquierdo = temporal->HijoIzquierdo;//Se mueve al nodo 5 (Hijo izquierdo de la raíz)
    temporalIzquierdo->HijoIzquierdo = nueva_raiz(4);//Crea el nodo 4 (Hijo izquierdo del nodo 5)
    temporalIzquierdo->HijoDerecho = nueva_raiz(6);//Crea el nodo 6 (Hijo derecho del nodo 5)
    
    temporalDerecho = temporalIzquierdo->HijoDerecho;//Se mueve al nodo 6 (Hijo derecho del nodo 5)
    temporalDerecho->HijoDerecho = nueva_raiz(7);//Crea el nodo 7 (Hijo derecho del nodo 6)
    
    //Sub-árbol derecho de la raíz
    temporal->HijoDerecho = nueva_raiz(9);//Crea el nodo 9 (Hijo derecho de la raiz)
    
    temporal = temporal->HijoDerecho;//Se mueve al nodo 9 (Hijo derecho de la raiz)
    temporal->HijoDerecho = nueva_raiz(13);//Crea el nodo 13 (Hijo derecho del nodo 9)
    
    temporal = temporal->HijoDerecho;//Se mueve al nodo 13 (Hijo derecho del nodo 9)
    temporal->HijoIzquierdo = nueva_raiz(11);//Crea el nodo 11 (Hijo izquierdo del nodo 13)
    
    temporal = temporal->HijoIzquierdo;//Se mueve al nodo 13 (Hijo del nodo 11)
    temporal->HijoIzquierdo = nueva_raiz(10);//Crea el nodo 10 (Hijo izquierdo del nodo 11)
    temporal->HijoDerecho = nueva_raiz(12);//Crea el nodo 12 (Hijo derecho del nodo 11)
  

}

Nodo *nueva_raiz(int valor){
  Nodo *raiz = NULL;
  raiz = malloc(sizeof(Nodo));

  raiz->valor = valor;
  raiz->HijoIzquierdo = NULL;
  raiz->HijoDerecho = NULL;
  
  return raiz;

}

void recorrer_arbol(Nodo *nodo){
    
    if (nodo != NULL){
        recorrer_arbol(nodo->HijoIzquierdo);
        recorrer_arbol(nodo->HijoDerecho);
        printf("%d  ", nodo->valor);
    }

}


void imprimir_arbol(){
    printf("Representación del árbol creado\n\n");
    printf("          _______8_______\n");
    printf("        5/               \\9_________\n");
    printf("   4___/ \\___6                      \\13\n");
    printf("              \\___7             11___/  \n");
    printf("                         10____/  \\____12\n\n\n");
}
