/**********************************************************************
    Instituto Tecnológico de Costa Rica
    Estructuras de Datos IC-2001
    II Semestre 2019
    Profesora: Samanta Ramijan Carmiol
    Laboratorio 6
    Estudiante: Randy Conejo Juárez
**********************************************************************/
//Definición de estructuras
typedef struct Nodo{
  int valor;
  struct Nodo *HijoIzquierdo;
  struct Nodo *HijoDerecho;
}Nodo;

typedef struct ArbolBinario{
  Nodo *raiz;
}ArbolBinario;

//Definición de funciones
/*-----------------------------------------------------------------------
	crear_arbol
	Entradas: No recibe parámetros
	Salidas: No retorna nada
	Funcionamiento: 
		- Crea un arbol binario insertando 8, 5, 4, 6, 7, 9, 13, 11, 10 y 12
		- La función crea especificamente un árbol con estos valores, es posible inplementar una función de insertar.
-----------------------------------------------------------------------*/
void crear_arbol();
/*-----------------------------------------------------------------------
	nueva_raiz
	Entradas: Recibe un número entero
	Salidas: Retorna un puntero de tipo Nodo
	Funcionamiento: 
		- Crea un nuevo Nodo con el valor que recibe de parámetro
-----------------------------------------------------------------------*/
Nodo *nueva_raiz(int valor);
/*-----------------------------------------------------------------------
	recorrer_arbol
	Entradas: Recibe un puntero de tipo Nodo el cual es la raíz del arbol
	Salidas: No retorna nada
	Funcionamiento: 
		- Recorre el arbol creado utilizando el recorrido postorden
-----------------------------------------------------------------------*/
void recorrer_arbol(Nodo *nodo);
/*-----------------------------------------------------------------------
	imprimir_arbol
	Entradas: No recibe parámetros
	Salidas: No retorna nada
	Funcionamiento: 
		- Imprime una representación del arból creado en la función de crear_arbol
-----------------------------------------------------------------------*/
void imprimir_arbol();
/*-----------------------------------------------------------------------
	main
	Entradas: No recibe parámetros
	Salidas: No retorna nada
	Funcionamiento: 
		- Llama a la función de crear_arbol
        - Llama a la función de imprimir_arbol
-----------------------------------------------------------------------*/
void main();
