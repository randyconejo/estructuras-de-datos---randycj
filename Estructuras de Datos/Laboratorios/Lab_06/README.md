**Laboratorio 6**


1. Investigue acerca de las distintas formas de recorrer o imprimir el contenido de un árbol binario, documente en el blog el funcionamiento de los recorridos de pre-orden, in-orden y post-orden.


2. Escriba en C, un programa capaz de imprimir un árbol binario sencillo realizando un recorrido post-orden.
