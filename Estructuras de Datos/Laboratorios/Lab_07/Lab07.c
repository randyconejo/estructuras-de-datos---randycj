#include <stdio.h>
#include "Lab7.h"
#include <stdlib.h>



int main() {
  menu();

  return 0;
}


void menu(){
  int eleccion;
  Nodo_persona *nuevo = NULL;
  nuevo = malloc(sizeof(Nodo_persona));
  int cantidad = 0;
  
  do {
    printf("\nMenu\n");
    printf("1. Agregar persona con amigos\n");
    printf("2. Mostrar personas\n");
    printf("0. Salir\n");
    printf("Ingrese su selección: ");
    scanf("%d", &eleccion);
    printf("\n");

    switch (eleccion){
      
      case 1:
          nuevo = crear_persona();
          cantidad ++;
          printf("Cantidad : %d\n", cantidad);

          agregar_persona(nuevo, cantidad);
          printf("Nueva persona: %s\n", ref_lista->persona->nombre);
          break;
      case 2:
            
        imprimir(cantidad);
          
        break;

    }
  }while(eleccion != 0);
}

Nodo_persona *crear_persona(){
  Nodo_persona *nuevo = NULL;
  nuevo = malloc(sizeof(Nodo_persona));
  Nodo_persona *temp = NULL;
  temp = malloc(sizeof(Nodo_persona));
  int eleccion;

  printf("Ingrese el nombre de la persona: ");
  scanf("%s", nuevo->nombre);
  nuevo->sig = NULL;
  
  printf("¿Desea agregar un amigo?\n");
  printf("1. SI\n");
  printf("0. NO\n");
  printf("Ingrese su selección: ");
  scanf("%d", &eleccion);

  temp = nuevo;
  if (eleccion == 1){
    do {
        temp->sig = agrega_amigo();
        temp = temp->sig;
        printf("Desea agregar otro amigo?\n");
        printf("1. SI\n");
        printf("0. NO\n");
        printf("Ingrese su selección: ");
        scanf("%d", &eleccion);
    }while (eleccion != 0);
  }
  return nuevo;

}

void agregar_persona(Nodo_persona *persona, int cantidad){
  ref_lista[cantidad -1].persona = persona;
}

void imprimir(int cantidad){
    int i = 0;
    Nodo_persona *temp = NULL;
    temp = malloc(sizeof(Nodo_persona));
    
    while (i < cantidad){
        temp = ref_lista[i].persona;
        printf("--------------------------------------\n");
        printf("Nombre: %s | Amigos: ", temp->nombre);
        temp = temp->sig;
        while (temp != NULL){
            printf("%s, ", temp->nombre);
            temp = temp->sig;
        }
        printf("\n");
        i ++;
    }

}

void imprimir_persona(Nodo_persona *persona){
  printf("Nombre: %s  ~  Amigos: ", persona->nombre);
  persona = persona->sig;

  while (persona != NULL){
    printf("%s, ", persona->nombre);
    persona = persona->sig;
  }
  printf("\n\n");
}

Nodo_persona *agrega_amigo(){
  Nodo_persona *amigo = NULL;
  amigo = malloc(sizeof(Nodo_persona));

  printf("Ingrese el nombre del amigo: ");
  scanf("%s", amigo->nombre);
  amigo->sig = NULL;

  return amigo;

}
