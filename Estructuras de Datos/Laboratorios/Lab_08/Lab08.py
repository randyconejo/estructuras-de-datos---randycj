class Nodo:
  
	def __init__(self, valor = None, siguiente = None):
		self.valor = valor
		self.siguiente = siguiente
   
	def actualizar_valor(self, valor):
		self.valor = valor
	   
	def actualizar_siguiente(self, Nodo):
		self.siguiente = Nodo
	   
class Lista_enlazada:
	
	def __init__(self):
		self.cabeza = None
	
	def agregar_inicio(self, valor):
		temp = Nodo(valor)
		temp.siguiente = self.cabeza
		self.cabeza = temp
		del temp
	
	def agregar_final(self,valor):
		temp = self.cabeza
		while (temp.siguiente != None):
			temp = temp.siguiente
		temp.siguiente = Nodo(valor)
		del temp
	
	def tamanno_lista(self):
		contador = 0
		temp = self.cabeza
		while (temp != None):
			temp = temp.siguiente
			contador += 1
		del temp
		return contador
		
	
	def imprimirLista(self):
		temp = self.cabeza
		while (temp != None):
			print(temp.valor , end=" ")
			temp = temp.siguiente
		del temp
		print("\n")
	
	def buscar_indice(self, indice):
		temp = self.cabeza
		contador = 0
		if indice >= self.tamanno_lista():
			return("El índice ingresado es mayor al tamaño de la lista")
			
			
		else:
			while contador != indice:
				temp = temp.siguiente
				contador += 1
			return temp.valor
	
	def borrar_lista(self):
		temp = self.cabeza
		while temp != None:
			del self.cabeza
			self.cabeza = temp.siguiente
			temp = temp.siguiente
		del temp
	
	def reverse(self):
		temp = self.cabeza
		self.borrar_lista()
		while temp != None:
			self.agregar_inicio(temp.valor)
			temp = temp.siguiente
		del temp
		
def main():
	miLista = Lista_enlazada()
	print("Agregamos a la lista los valores en el siguiente orden, 5 al inicio, 8 al inicio y 7 al final")
	miLista.agregar_inicio(5)
	miLista.agregar_inicio(8)
	miLista.agregar_final(7)
	print("Se imprime la lista")
	miLista.imprimirLista()
	print("\nTamaño de la lista: ", miLista.tamanno_lista())
	print("Solicitamos al usuario un índice para ver que valor contiene el nodo")
	print(miLista.buscar_indice(int(input("Inserte el índice deseado: "))))
	print("Borramos todos los nodos de la lista e imprimimos el tamaño para verificar que no hay nungún elemento")
	miLista.borrar_lista()
	print("\nTamaño de la lista: ", miLista.tamanno_lista())
	print("Volvemos a añadir los nodos a la lista")
	print("Orden original")
	miLista.agregar_inicio(5)
	miLista.agregar_inicio(8)
	miLista.agregar_final(7)
	miLista.imprimirLista()
	print("Le damos vuelta a los valores de los nodos")
	miLista.reverse()
	print("Nuevo orden de los valores")
	miLista.imprimirLista()

main()
