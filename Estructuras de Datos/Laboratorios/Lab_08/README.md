**Laboratorio 8**

Programe en python lo siguiente.

Clase Nodo, con los atributos:

1. valor
2. siguiente

con los métodos:

1. Actualizar valor
2. Actualizar siguiente


Clase Lista, con los atributos:

1. cabeza

con los métodos:

1. Agregar al inicio
2. Agregar al final
3. Imprimir contenido
4. Tamaño de la lista
5. Indice (retorna el elemento en la posición dada)
6. Limpiar (elimina todos los elementos de la lista)
7. Reverse (da la vuelta a la lista, el primer elemento ahora es el último)
