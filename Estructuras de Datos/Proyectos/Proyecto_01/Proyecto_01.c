#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "Proyecto_01.h"

//Declaración de estructuras globales a usar
CartaActual *carta_actual = NULL;

void main(){
    
    carta_actual = malloc(sizeof(CartaActual));
    crear_cartas();
    //imprimir_cartas();
    menu();
    
    return;
}


void crear_cartas(){
    int contador = 1;
    int contador_auxiliar = 1;
    Carta *temporal = NULL;
    Carta *nueva_carta = NULL;
    nueva_carta = malloc(sizeof(Carta));
    temporal = malloc(sizeof(Carta));
    
    while (contador <= 52){
    
        
        while (contador_auxiliar <= 13){//Se usan dos contadores, uno para el valor y otro para el valor total.
        
            nueva_carta = carta_nueva(contador, contador_auxiliar);//crea una variable de tipo carta
            if (contador == 1){
                temporal = nueva_carta;
                carta_actual->actual = nueva_carta;
                carta_actual->cabeza = nueva_carta;
                carta_actual->posicion_actual = 0;
            }
            else if (contador == 52){
                nueva_carta->sig=NULL;
                temporal->sig = nueva_carta;
                nueva_carta->ant = temporal;
            }
            else{
                temporal->sig = nueva_carta;
                nueva_carta->ant = temporal;
                temporal = nueva_carta;
            }
            contador ++;
            contador_auxiliar ++;
        }
    contador_auxiliar = 1;
    }
           
}

            
Carta *carta_nueva(int valor_total, int valor){
    Carta *nueva_carta = NULL;
    
    nueva_carta = malloc(sizeof(Carta));
    nueva_carta->valor_total = valor_total;
    nueva_carta->valor = valor;
    if (valor_total == 1){
        nueva_carta->ant = NULL;
    }
    return nueva_carta;
}
    
    
void imprimir_cartas(){
    Carta *temporal = NULL;
    temporal = malloc(sizeof(Carta));
    temporal = carta_actual->actual;
    
    while (temporal != NULL){
        printf("Valor : %d  Valor Total: %d\n", temporal->valor, temporal->valor_total);
        temporal = temporal->sig;
    }
    return;
}

void mostrar_actual(Carta *actual){

    int posicion = actual->valor_total;
    
    if (posicion <=13){
        if (actual->valor == 1){
            printf(WHITE "\t.------.\n\t|A .   |\n\t| / \\  |\n\t|(_,_) |\n\t|  I  A|\n\t'------'" RESET "\n");
        }
        else if(actual->valor == 10){
            printf(WHITE "\t.------.\n\t|10.   |\n\t| / \\  |\n\t|(_,_) |\n\t|  I 10|\n\t'------'" RESET "\n");
        }
        else if (actual->valor == 11){
            printf(WHITE "\t.------.\n\t|J .   |\n\t| / \\  |\n\t|(_,_) |\n\t|  I  J|\n\t'------'" RESET "\n");
        }
        else if(actual->valor == 12){
            printf(WHITE "\t.------.\n\t|Q .   |\n\t| / \\  |\n\t|(_,_) |\n\t|  I  Q|\n\t'------'" RESET "\n");
        }
        else if(actual->valor == 13){
            printf(WHITE "\t.------.\n\t|K .   |\n\t| / \\  |\n\t|(_,_) |\n\t|  I  K|\n\t'------'" RESET "\n");
        }
        else {
            printf(WHITE "\t.------.\n\t|%d .   |\n\t| / \\  |\n\t|(_,_) |\n\t|  I  %d|\n\t'------'" RESET "\n", actual->valor, actual->valor);
        }
    }
    
    else if(posicion <= 26){
        if (actual->valor == 1){
            printf(RED "\t.------.\n\t|A_  _ |\n\t|( \\/ )|\n\t| \\  / |\n\t|  \\/ A|\n\t'------'" RESET "\n");
        }
        else if(actual->valor == 10){
            printf(RED "\t.------.\n\t|10  _ |\n\t|( \\/ )|\n\t| \\  / |\n\t|  \\/10|\n\t'------'" RESET "\n");
        }
        else if (actual->valor == 11){
            printf(RED "\t.------.\n\t|J_  _ |\n\t|( \\/ )|\n\t| \\  / |\n\t|  \\/ J|\n\t'------'" RESET "\n");
        }
        else if(actual->valor == 12){
            printf(RED "\t.------.\n\t|Q_  _ |\n\t|( \\/ )|\n\t| \\  / |\n\t|  \\/ Q|\n\t'------'" RESET "\n");
        }
        else if(actual->valor == 13){
            printf(RED "\t.------.\n\t|K_  _ |\n\t|( \\/ )|\n\t| \\  / |\n\t|  \\/ K|\n\t'------'" RESET "\n");
        }
        else {
            printf(RED "\t.------.\n\t|%d_  _ |\n\t|( \\/ )|\n\t| \\  / |\n\t|  \\/ %d|\n\t'------'" RESET "\n", actual->valor, actual->valor);
        }
    }
        
    else if (posicion <= 39){
        if (actual->valor == 1){
            printf(BLUE "\t.------.\n\t|A /\\  |\n\t| /  \\ |\n\t| \\  / |\n\t|  \\/ A|\n\t'------'" RESET "\n");
        }
        else if (actual->valor == 10){
            printf(BLUE "\t.------.\n\t|10/\\  |\n\t| /  \\ |\n\t| \\  / |\n\t|  \\/10|\n\t'------'" RESET "\n");
        }
        else if (actual->valor == 11){
            printf(BLUE "\t.------.\n\t|J /\\  |\n\t| /  \\ |\n\t| \\  / |\n\t|  \\/ J|\n\t'------'" RESET "\n");
        }
        else if (actual->valor == 12){
            printf(BLUE "\t.------.\n\t|Q /\\  |\n\t| /  \\ |\n\t| \\  / |\n\t|  \\/ Q|\n\t'------'" RESET "\n");
        }
        else if (actual->valor == 13){
            printf(BLUE "\t.------.\n\t|K /\\  |\n\t| /  \\ |\n\t| \\  / |\n\t|  \\/ K|\n\t'------'" RESET "\n");
        }
        else {
            printf(BLUE "\t.------.\n\t|%d /\\  |\n\t| /  \\ |\n\t| \\  / |\n\t|  \\/ %d|\n\t'------'" RESET "\n", actual->valor, actual->valor);
        }
    }
        
    else {
        if (actual->valor == 1){
            printf(GREEN "\t.------.\n\t|A _   |\n\t| ( )  |\n\t|(_x_) |\n\t|  Y  A|\n\t'------'" RESET "\n");
        }
        else if (actual->valor == 10){
            printf(GREEN "\t.------.\n\t|10_   |\n\t| ( )  |\n\t|(_x_) |\n\t|  Y 10|\n\t'------'" RESET "\n");
        }
        else if (actual->valor == 11){
            printf(GREEN "\t.------.\n\t|J _   |\n\t| ( )  |\n\t|(_x_) |\n\t|  Y  J|\n\t'------'" RESET "\n");
        }
        else if (actual->valor == 12){
            printf(GREEN "\t.------.\n\t|Q _   |\n\t| ( )  |\n\t|(_x_) |\n\t|  Y  Q|\n\t'------'" RESET "\n");
        }
        else if (actual->valor == 13){
            printf(GREEN "\t.------.\n\t|K _   |\n\t| ( )  |\n\t|(_x_) |\n\t|  Y  K|\n\t'------'" RESET "\n");
        }
        else {
            printf(GREEN "\t.------.\n\t|%d _   |\n\t| ( )  |\n\t|(_x_) |\n\t|  Y  %d|\n\t'------'" RESET "\n", actual->valor, actual->valor);
        }
    }

    
    
    
}


void carta_siguiente(){
    carta_actual->actual = carta_actual->actual->sig;
    carta_actual->posicion_actual ++;
}

void carta_anterior(){
    carta_actual->actual = carta_actual->actual->ant;
    carta_actual->posicion_actual --;
}

void barajar(){
    int numero;
    int baraja[RANGO];
    int i = 0;
    int bandera;
    int familia;
    Carta *temporal = NULL;
    temporal = malloc(sizeof(Carta));
    srand(time(NULL));
    
    while (i<RANGO){
        bandera = 0;
        numero = (rand() % RANGO) + 1;
        if (i == 0){
            baraja[i] = numero;
            i ++;
        }
        else{
            for (int j=0; j<i; j++){
                if (baraja[j] != numero){
                    continue;
                }
                else{
                    bandera = 1;
                    break;
                }
            }
            if (bandera == 0){
                baraja[i] = numero;
                i ++;
            }
        }
    }    
    
    temporal = carta_actual->cabeza;
    
    for (int i=0; i<RANGO; i++){
        if (baraja[i] <= 13){
            familia = 0;
        }
        else if (baraja[i] <= 26){
            familia = 13;
        }
        else if (baraja[i] <= 39){
            familia = 26;
        }
        else {
            familia = 39;
        }
        
        temporal->valor_total = baraja[i];
        temporal->valor = baraja[i] - familia;
        temporal = temporal->sig;
    }

}

int sacarPivote(int *lista, int izq, int der){
    int i;
    int pivote, valor_pivote;
    int aux;

    pivote = izq;
    valor_pivote = lista[pivote];
    for (i=izq+1; i<=der; i++){
        if (lista[i] < valor_pivote){
                pivote++;
                aux = lista[i];
                lista[i] = lista[pivote];
                lista[pivote] = aux;

        }
    }
    aux = lista[izq];
    lista[izq] = lista[pivote];
    lista[pivote] = aux;
    return pivote;
}

void Quicksort(int *lista, int izq, int der){
     int pivote;
     if(izq < der){
        pivote = sacarPivote(lista, izq, der);
        Quicksort(lista, izq, pivote-1);
        Quicksort(lista, pivote+1, der);
     }
}
void ordenar(int *lista){
     
     int familia;
     
     Quicksort(lista,0,51);
     
     struct Carta *temporal = carta_actual->cabeza;
     
     for (int i=0; i<52; i++){
        if (lista[i] <= 13){
            familia = 0;
        }
        else if (lista[i] <= 26){
            familia = 13;
        }
        else if (lista[i] <= 39){
            familia = 26;
        }
        else {
            familia = 39;
        }
        
        temporal->valor_total = lista[i];
        temporal->valor = lista[i] - familia;
        temporal = temporal->sig;
    }    

}

void menu(){
    int eleccion;
    Carta *temporal = NULL;
    temporal = malloc(sizeof(Carta));
    int lista[52];
    int cont;
    do{
        printf("\n\t" WCYAN "Menú Principal" RESET "\n");
        printf(CYAN " ________________________________\n");
        printf(CYAN "|                                |\n");
        printf(CYAN "| 1.\tMostrar Carta Actual     |\n");
        printf(CYAN "| 2.\tMostrar Siguiente Carta  |\n");
        printf(CYAN "| 3.\tMostrar Carta Anterior   |\n");
        printf(CYAN "| 4.\tMostrar toda la baraja   |\n");
        printf(CYAN "| 5.\tOrdenar Baraja           |\n");
        printf(CYAN "| 6.\tBarajar                  |\n");
        printf(CYAN "|" RESET);
        printf(RED " 0.\tSalir                    " RESET);
        printf(CYAN "|\n");
        printf(CYAN ".________________________________." RESET "\n");
        printf("\nIngrese su elección: ");
        scanf("%d", &eleccion);
        printf("\n");
        
        switch (eleccion){
            
            case 1:
                mostrar_actual(carta_actual->actual);
                representacion_baraja();
                break;
            
            case 2:
                if (carta_actual->posicion_actual == 51){
                    printf("\nLlegó al final de la baraja\n");
                    joker();
                }
                else{
                    carta_siguiente();
                    mostrar_actual(carta_actual->actual);
                    representacion_baraja();
                }
                break;
            
            case 3:
                if (carta_actual->posicion_actual == 0){
                    printf("\nLlegó al inicio de la baraja\n");
                    joker();
                }
                else{
                    carta_anterior();
                    mostrar_actual(carta_actual->actual);
                    representacion_baraja();
                }
                break;
            
            case 4: 
                temporal = carta_actual->cabeza;
                printf("________________________________\n\n");
                joker();
                while (temporal != NULL) {
                    mostrar_actual(temporal);
                    temporal = temporal->sig;
                }
                joker();
                printf("\n________________________________\n");
                break;
                
            case 5:
                temporal = carta_actual->cabeza;
                cont=0;
                while (temporal != NULL) {
                    lista[cont]=temporal->valor_total;
                    temporal = temporal->sig;
                    cont++;
                }
                ordenar(lista);
                printf("Se ha ordenado el mazo de cartas\n");
                carta_actual->actual = carta_actual->cabeza;
                carta_actual->posicion_actual = 0;
                break;
                    
            case 6:
                barajar();
                printf("Se ha barajado el mazo de cartas\n");
                carta_actual->actual = carta_actual->cabeza;
                carta_actual->posicion_actual = 0;
                break;

        }
    }while(eleccion != 0);
}

void representacion_baraja(){
    printf(BLUE "\n              __                         \n" RESET);
    printf("        %s%s%s", WHITE  "_..-'" RESET, BLUE "'--'----" RESET, RED "_.          \n" RESET); 
    printf("      %s%s%s", WHITE "''.-''" RESET, BLUE " | .---" RESET, RED "/ _`-._       \n" RESET);
    printf(GREEN "    ,' %s%s%s" RESET, WHITE "\\ \\  ;" RESET, BLUE "| | ," RESET, RED "/ / `-._`-.  \n" RESET);
    printf(GREEN "  ,' ,',%s%s%s" RESET, WHITE "\\ \\( " RESET, BLUE "| |/" RESET, RED "/ /,-._  / /  \n" RESET);
    printf(GREEN "   .`. `,%s%s%s" RESET, WHITE "\\ \\`"RESET , BLUE "| |" RESET, RED "/ / |   )/ /   \n" RESET);
    printf(GREEN " / /`_`.\\_%s%s%s%s%s" RESET, WHITE "\\ \\" RESET, BLUE "| " RESET, RED "/" RESET, "_.-.", RED "'-''/ /  \n" RESET);//5%s
    printf(GREEN "/ /_|_:.`. %s%s%s%s" RESET, WHITE "\\ " RESET, BLUE "|" RESET, ";'`..')", RED "  / /      \n" RESET);
    printf(GREEN "`-._`-._`.`.%s%s" RESET, WHITE ";`    ,'" RESET, RED "  / /       \n" RESET);
    printf(GREEN "    `-._`.`%s%s" RESET, WHITE "/    ,'" RESET, RED "-._/ /         \n" RESET);
    printf("      %s%s%s%s", WHITE ":" RESET, GREEN " `-" RESET, WHITE "/     \\" RESET, RED "`-.._/         \n" RESET);
    printf("      |  :      ;._ (                      \n");
    return;
}

void joker(){
    printf(YELLOW "\n            .=.   \n");
    printf("       _   //(`)_\n");
    printf("      //`\\/ |\\ 0`\\\\\n");
    printf("      ||-.\\_|_/.-||\n");
    printf("      )/ |_____| \\(\n");
    printf("     0   #/\\ /\\#  0   \n");
    printf("        _| o o |_\n");
    printf("       ((|, ^ ,|))\n");
    printf("        `||\\_/||`\n");
    printf("         || _ ||      \n");
    printf("         | \\_/ |     \n");
    printf("     0.__.\\   /.__.0\n");
    printf("      `._  `''`  _.'\n");
    printf("         / ;  \\ \\\n");
    printf("       0'-' )/`'-0\n");
    printf("           0`\n" RESET);
}
