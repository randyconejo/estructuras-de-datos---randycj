/*********************************************************************
    Instituto Tecnológico de Costa Rica
    Programación Orientada a Objetos IC-2101
    II Semestre 2019
    Profesora: Samanta Ramijan Carmiol
    
    Estudiantes: 
    -Kevin Zumbado Cruz (2019258634)
    -Randy Conejo Juarez (2019066448)
    
    Laboratorio 6
    Para probar este código en línea, visite el siguiente link: https://repl.it/@RandyCJ/Lab-06
**********************************************************************/
public class Abanico extends Dispositivo{
  private int velocidad;
  public Abanico(){
    super("Abanico");
    this.velocidad = 1;
  }

  public void cambiarVelocidad(){
    switch (velocidad){
      case 1:
        this.velocidad = 2;
        break;
      case 2:
        this.velocidad = 3;
        break;
      case 3:
        this.velocidad = 1;
        break;
    }
  }
  
  public String toString(){
    if (super.getEstado()=="Encendido"){
      return "\nNombre: "+ getClass().getName() +"\nEstado: "+ super.getEstado()+"\nVelocidad: "+velocidad;
    }else{
      return "\nNombre: "+ getClass().getName() +"\nEstado: "+ super.getEstado();
    }
  }

}