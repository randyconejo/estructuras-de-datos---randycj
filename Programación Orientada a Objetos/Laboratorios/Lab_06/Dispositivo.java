/*********************************************************************
    Instituto Tecnológico de Costa Rica
    Programación Orientada a Objetos IC-2101
    II Semestre 2019
    Profesora: Samanta Ramijan Carmiol
    
    Estudiantes: 
    -Kevin Zumbado Cruz (2019258634)
    -Randy Conejo Juarez (2019066448)
    
    Laboratorio 6
    Para probar este código en línea, visite el siguiente link: https://repl.it/@RandyCJ/Lab-06
**********************************************************************/
public abstract class Dispositivo implements Encendible{

  private String aparato;
  private boolean estado;

  public Dispositivo(String aparato){
    this.aparato = aparato;
    this.estado = false;
  }

  public String getAparato(){
    return aparato;
  }

  public String getEstado(){
    if (estado == true){
    return "Encendido";
    }
    else{return "Apagado";}
  }

  @Override
  public void encender(){
    this.estado=true;
  }
  @Override
  public void apagar(){
    this.estado=false;
  }
}