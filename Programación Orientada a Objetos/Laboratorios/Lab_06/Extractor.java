/*********************************************************************
    Instituto Tecnológico de Costa Rica
    Programación Orientada a Objetos IC-2101
    II Semestre 2019
    Profesora: Samanta Ramijan Carmiol
    
    Estudiantes: 
    -Kevin Zumbado Cruz (2019258634)
    -Randy Conejo Juarez (2019066448)
    
    Laboratorio 6
    Para probar este código en línea, visite el siguiente link: https://repl.it/@RandyCJ/Lab-06
**********************************************************************/
public class Extractor extends Dispositivo{

  public Extractor(){
    super("Extractor de Grasa");
  }

  public String toString(){
    return "\nNombre: "+ super.getAparato() +"\nEstado: "+ super.getEstado();
  }

}