/*********************************************************************
    Instituto Tecnológico de Costa Rica
    Programación Orientada a Objetos IC-2101
    II Semestre 2019
    Profesora: Samanta Ramijan Carmiol
    
    Estudiantes: 
    -Kevin Zumbado Cruz (2019258634)
    -Randy Conejo Juarez (2019066448)
    
    Laboratorio 6
    Para probar este código en línea, visite el siguiente link: https://repl.it/@RandyCJ/Lab-06
**********************************************************************/
public class Lavadora extends Dispositivo{
  private int ciclosDeLavado;

  public Lavadora(){
    super("Lavadora");
    this.ciclosDeLavado = 1;
  }

  public void cambiarCiclos(){
    switch (ciclosDeLavado){
      case 1:
        this.ciclosDeLavado = 2;
        break;
      case 2:
        this.ciclosDeLavado = 3;
        break;
      case 3:
        this.ciclosDeLavado = 1;
        break;
    }
  }
  
  public String toString(){
    if (super.getEstado()=="Encendido"){
      return "\nNombre: "+ getClass().getName() +"\nEstado: "+ super.getEstado()+ "\nCiclo de Lavado: "+ ciclosDeLavado;
    }else{
      return "\nNombre: "+ getClass().getName() +"\nEstado: "+ super.getEstado();
    }
  }
}