/*********************************************************************
    Instituto Tecnológico de Costa Rica
    Programación Orientada a Objetos IC-2101
    II Semestre 2019
    Profesora: Samanta Ramijan Carmiol
    
    Estudiantes: 
    -Kevin Zumbado Cruz (2019258634)
    -Randy Conejo Juarez (2019066448)
    
    Laboratorio 6
    Para probar este código en línea, visite el siguiente link: https://repl.it/@RandyCJ/Lab-06
**********************************************************************/
public class Secadora extends Dispositivo{
  private int ciclosDeSecado;

  public Secadora(){
    super("Secadora");
    this.ciclosDeSecado = 1;
  }

  public void cambiarCiclos(){
    switch (ciclosDeSecado){
      case 1:
        this.ciclosDeSecado = 2;
        break;
      case 2:
        this.ciclosDeSecado = 3;
        break;
      case 3:
        this.ciclosDeSecado = 1;
        break;
    }
  }

  public String toString(){
    if (super.getEstado()=="Encendido"){
      return "\nNombre: "+ getClass().getName() +"\nEstado: "+ super.getEstado()+"\nCiclo de Secado: "+ciclosDeSecado;
    }else{
      return "\nNombre: "+ getClass().getName() +"\nEstado: "+ super.getEstado();
    }
  }
}