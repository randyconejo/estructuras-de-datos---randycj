package lab08;

import java.util.ArrayList;
import java.util.Date;

public class CasoContinuo {
    private String tipoDeCaso;
    private String horaInicio;
    private String horaFinal;
    private ArrayList<String> horario;
    
    public CasoContinuo(String tipoDeCaso, String horaInicio, String horaFinal, ArrayList<String> horario){
        this.tipoDeCaso = tipoDeCaso;
        this.horaInicio = horaInicio;
        this.horaFinal = horaFinal;
        this.horario = horario;
    }
    
    @Override
    public String toString(){
        return "Tipo de caso: " + this.tipoDeCaso + "\nDias de Trabajo:  " + getDias(horario.size(), 0) + "\nHora de Inicio: " + this.horaInicio + " | Hora Final: " + this.horaFinal;
    }
    
    public String getDias(int contador, int contaux){
        if (contador != 0){
            return horario.get(contaux) + "  " + getDias(contador-1, contaux+1);
        }
        
        return "";
    }
}
