package lab08;

import java.util.ArrayList;
import java.util.Date;

public class CasoParticular {
    private String tipoDeCaso;
    private String horaInicio;
    private String horaFinal;
    private String fecha;
    
    public CasoParticular(String tipoDeCaso, String horaInicio, String horaFinal, String fecha){
        this.tipoDeCaso = tipoDeCaso;
        this.horaInicio = horaInicio;
        this.horaFinal = horaFinal;
        this.fecha = fecha;
    }
    
    @Override
    public String toString(){
        return "Tipo de caso: " + this.tipoDeCaso + "\nFecha: " + this.fecha + "\nHora de Inicio: " + this.horaInicio + " | Hora Final: " + this.horaFinal;
    }
    
}
