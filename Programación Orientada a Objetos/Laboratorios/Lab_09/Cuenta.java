package lab08;

public class Cuenta {
    private Persona propietario;
    private int contraseña;
    
    public Cuenta(String nombre, String apellido, int ID, String tipoCuenta){
        if (tipoCuenta == "Oficial"){
            this.propietario = new Oficial(nombre, apellido, ID);
            this.contraseña = 1234;
        }
        else {
            this.propietario = new Administrador(nombre, apellido, ID);
            this.contraseña = 1234;
        }
    }
    
    public int getID(){
        return propietario.getID();
    }
    
    public String getNombre(){
        return propietario.getNombre();
    }
 
    public String getApellido(){
        return propietario.getApellido();
    }
    
    public Object getPropietario(){
        return this.propietario;
    }
    
    @Override
    public String toString(){
        return "Nombre: " + propietario.getNombre() + "\nApellido: " + propietario.getApellido() + "\nID: " + propietario.getID();
    }
    
    public boolean verificarContraseña(int contraseña){
        return this.contraseña == contraseña;
    }
    
    public void setContraseña(int nuevaContraseña){
        this.contraseña = nuevaContraseña;
    }
}
