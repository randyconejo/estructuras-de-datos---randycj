package lab08;
import java.util.Scanner;
//https://repl.it/@RandyCJ/Lab-09

class Main{
    
    public static void main(String[] args){
        
        Sistema sistema = new Sistema();
        Scanner entrada = new Scanner(System.in);
        int eleccion;
        int ID;
        
        sistema.agregarCuentaOficial("Manuel", "Rodriguez", 4922);
        sistema.agregarCuentaAdmin("María", "Suarez", 3220);
        sistema.agregarCuentaOficial("Pedro", "Corrales", 2175);
        
        /*System.out.println(sistema.getCuenta(0, "Oficial").toString());
        System.out.println(sistema.getCuenta(1, "Oficial").toString());
        System.out.println(sistema.getCuenta(0, "Admin").toString());*/
        
        do {
            
            System.out.println("Bienvenido al sistema");
            System.out.println("(1) Iniciar Sesión");
            System.out.println("(0) Salir");
            System.out.print("Ingrese su selección: ");
            eleccion = entrada.nextInt();
        
            if (eleccion == 1){
                System.out.print("Ingrese su ID: ");
                ID = entrada.nextInt();
                sistema.iniciarSesion((ID));
                sistema.menu();
            }
        }while(eleccion != 0);
    }
}
