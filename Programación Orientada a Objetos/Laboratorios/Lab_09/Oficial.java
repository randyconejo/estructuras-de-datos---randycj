package lab08;

import java.util.ArrayList;

public class Oficial extends Persona {
   private ArrayList<CasoContinuo> casosContinuos;
   private ArrayList<CasoParticular> casosParticulares;
   
   public Oficial(String nombre, String apellido, int ID){
       super(nombre, apellido, ID);
       this.casosContinuos = new ArrayList<>();
       this.casosParticulares = new ArrayList<>();
   }
   
   public ArrayList<CasoContinuo> getCasosContinuos(){
        return this.casosContinuos;
   }
   
   public ArrayList<CasoParticular> getCasosParticulares(){
       return this.casosParticulares;
   }
  
   
    public void verCasosParticulares(){
        if (casosParticulares.isEmpty()){
           System.out.println("\nNo tiene ningún caso particular asignado\n");
           return;
        }
        for (int i = 0; i<casosParticulares.size(); i++){
           System.out.println("-------------------------------------\nCaso " + (i+1));
           System.out.println(casosParticulares.get(i).toString());
        }
        System.out.println("-------------------------------------\n");
    }

    public void verCasosContinuos(){
        if (casosContinuos.isEmpty()){
           System.out.println("\nNo tiene ningún caso continuo asignado\n");
           return;
        }
        for (int i = 0; i<casosContinuos.size(); i++){
           System.out.println("-------------------------------------\nCaso " + (i+1));
           System.out.println(casosContinuos.get(i).toString());
        }
        System.out.println("-------------------------------------\n");
    }


   
}
