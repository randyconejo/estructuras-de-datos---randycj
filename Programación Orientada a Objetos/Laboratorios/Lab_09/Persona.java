package lab08;

public abstract class Persona {
    private String nombre;
    private String apellido;
    private int ID;
    
    public Persona(){
        this.nombre = "NULL";
        this.apellido = "NULL";
        this.ID = 0;
    }
    
    public Persona(String nombre, String apellido, int ID){
        this.nombre = nombre;
        this.apellido = apellido;
        this.ID = ID;
    }
    
    public String getNombre(){
        return this.nombre;
    }
    
    public String getApellido(){
        return this.apellido;
    }
    
    public int getID(){
        return this.ID;
    }
}
