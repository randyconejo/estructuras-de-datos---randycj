
package lab08;

import java.util.ArrayList;
import java.util.Scanner;

public class Sistema {
    private ArrayList<Cuenta> cuentasOficiales;
    private ArrayList<Cuenta> cuentasAdmins;
    private Cuenta cuentaActual;
    
    Scanner entrada = new Scanner(System.in);
    
    public Sistema(){
        this.cuentasOficiales = new ArrayList<>();
        this.cuentasAdmins = new ArrayList<>();
    }
    
    public void agregarCuentaOficial(String nombre, String apellido, int ID){
        cuentasOficiales.add(new Cuenta(nombre, apellido, ID, "Oficial"));
    }
    
    public void agregarCuentaAdmin(String nombre, String apellido, int ID){
        cuentasAdmins.add(new Cuenta(nombre, apellido, ID, "Admin"));
    }
    
    public int buscarCuenta(int ID, String tipoCuenta){
        if (tipoCuenta == "Oficial"){
            for (int i = 0; i<cuentasOficiales.size(); i++){
                if (cuentasOficiales.get(i).getID() == ID){
                    return i;
                }
            }
        }
        else {
            for (int i = 0; i<cuentasAdmins.size(); i++){
                if (cuentasAdmins.get(i).getID() == ID){
                    return i;
                }
            }
        }
        return 100;
        
    }
    
    public Cuenta getCuenta(int posicion, String tipoCuenta){
        if (tipoCuenta == "Oficial"){
            return cuentasOficiales.get(posicion);
        }
        else {
            return cuentasAdmins.get(posicion);
        }
    }
    
    public void iniciarSesion(int ID){
        int contraseña;
        
        if (buscarCuenta(ID, "Oficial") <100){
            this.cuentaActual = getCuenta(buscarCuenta(ID, "Oficial"), "Oficial");
            System.out.print("Ingrese su contraseña: ");
            contraseña = entrada.nextInt();
            
            while (!this.cuentaActual.verificarContraseña(contraseña)){
                System.out.println("\nContraseña Incorrecta. Por favor ingrese nuevamente su contraseña");
                contraseña = entrada.nextInt();
            }
            System.out.println("\nIngresó al sistema exitosamente");
            System.out.println("\nHola " + this.cuentaActual.getNombre() + " " + this.cuentaActual.getApellido() + "\n");
            
        }
        else if (buscarCuenta(ID, "Admin") <100){
            this.cuentaActual = getCuenta(buscarCuenta(ID, "Admin"), "Admin");
            System.out.print("Ingrese su contraseña: ");
            contraseña = entrada.nextInt();
            
            while (!this.cuentaActual.verificarContraseña(contraseña)){
                System.out.println("\nContraseña Incorrecta. Por favor ingrese nuevamente su contraseña");
                contraseña = entrada.nextInt();
            }
            System.out.println("\nIngresó al sistema exitosamente");
            System.out.println("\nHola " + this.cuentaActual.getNombre() + " " + this.cuentaActual.getApellido() + "\n");
        }
        else {
            System.out.println("Ingresó un ID incorrecto");
            System.out.print("Ingrese nuevamente su ID: ");
            ID = entrada.nextInt();
            iniciarSesion(ID);
            
        }
        
    }
    
    public Cuenta getCuentaActual(){
            return this.cuentaActual;
    }
    
    public void menu(){
        int eleccion;
        
        if (cuentaActual.getPropietario() instanceof Oficial){
            System.out.println("Menú Principal");
            System.out.println("(1) Ver mis casos continuos");
            System.out.println("(2) Ver mis casos particulares");
            System.out.println("(3) Cambiar contraseña");
            System.out.println("(0) Cerrar Sesión");
            System.out.print("Ingrese su selección: ");
            eleccion = entrada.nextInt();
            if (eleccion == 0){
                return;
            }
            menuOficial(eleccion);
        }
        else {
            System.out.println("Menú Principal");
            System.out.println("(1) Asignar caso continuo");
            System.out.println("(2) Asignar caso particular");
            System.out.println("(3) Cambiar contraseña");
            System.out.println("(0) Cerrar Sesión");
            System.out.print("Ingrese su selección: ");
            eleccion = entrada.nextInt();
            if (eleccion == 0){
                return;
            }
            menuAdmin(eleccion);
        }
    }
    
    private void menuOficial(int eleccion){
        Oficial persona = ((Oficial) cuentaActual.getPropietario());
        
        switch (eleccion){
            
            case 1:
                persona.verCasosContinuos();
                break;
                
            case 2:
                persona.verCasosParticulares();
                break;
                
            case 3:
                cambiarContraseña();
                break;
                
        }
        menu();
    }
    
    private void menuAdmin(int eleccion){
        Administrador admin = ((Administrador) cuentaActual.getPropietario());
        
        switch (eleccion){
            
            case 1:
                addCasoContinuo();
                break;
            
            case 2:
                addCasoParticular();
                break;   
            
            case 3:
                cambiarContraseña();
                break;
        }
        menu();
    }
    
    private void addCasoContinuo(){
        int ID;
        String tipoDeCaso;
        String horaInicio;
        String horaFinal;
        ArrayList<String> horario;
        String dia;
        Oficial oficial;
        horario = new ArrayList<String>();
        
        System.out.println("Ingrese el ID del oficial");
        ID = entrada.nextInt();
        
        if (buscarCuenta(ID, "Oficial") < 100){
            oficial = ((Oficial) getCuenta(buscarCuenta(ID, "Oficial"), "Oficial").getPropietario());
            System.out.print("\nIngrese el tipo de caso: ");
            tipoDeCaso = entrada.nextLine();
            tipoDeCaso = entrada.nextLine();
            System.out.println("\nIngrese los días de trabajo (L, K, M, J, V, S, D), Escriba SALIR para terminar");
            System.out.print("Dias: ");
            dia = entrada.nextLine();
            do{
                horario.add(dia);
                System.out.print("Dias: ");
                dia = entrada.nextLine();
            }while(!"SALIR".equals(dia));
            System.out.print("\nIngrese la hora de inicio: ");
            horaInicio = entrada.nextLine();
            System.out.print("\nIngrese la hora final: ");
            horaFinal = entrada.nextLine();
            oficial.getCasosContinuos().add(new CasoContinuo(tipoDeCaso, horaInicio, horaFinal, horario));
        }
        else {
            System.out.println("No existe oficial con ese número de ID");
            addCasoContinuo();
        }
    }
    
    private void addCasoParticular(){
        int ID;
        String tipoDeCaso;
        String horaInicio;
        String horaFinal;
        String fecha;
        Oficial oficial;
        
        System.out.println("Ingrese el ID del oficial");
        ID = entrada.nextInt();
        
        if (buscarCuenta(ID, "Oficial") < 100){
            oficial = ((Oficial) getCuenta(buscarCuenta(ID, "Oficial"), "Oficial").getPropietario());
            System.out.print("\nIngrese el tipo de caso: ");
            tipoDeCaso = entrada.nextLine();
            tipoDeCaso = entrada.nextLine();
            System.out.println("\nIngrese la fecha del caso (DD/MM/AAAA)");
            fecha = entrada.nextLine();
            System.out.print("\nIngrese la hora de inicio: ");
            horaInicio = entrada.nextLine();
            System.out.print("\nIngrese la hora final: ");
            horaFinal = entrada.nextLine();
            oficial.getCasosParticulares().add(new CasoParticular(tipoDeCaso, horaInicio, horaFinal, fecha));
        }
        else {
            System.out.println("No existe oficial con ese número de ID");
            addCasoParticular();
        }
    }
    
    private void cambiarContraseña(){
        int actual;
        System.out.println("Por favor ingrese su contraseña actual");
        actual = entrada.nextInt();
        if (cuentaActual.verificarContraseña(actual) == false){
            System.out.println("Contraseña incorrecta");
            cambiarContraseña();
            return;
        }
        else {
            System.out.println("Ingrese su nueva contraseña: ");
            actual = entrada.nextInt();
            cuentaActual.setContraseña(actual);
            System.out.println("Contraseña cambiada exitosamente\n");
        }
        
    }    
}
