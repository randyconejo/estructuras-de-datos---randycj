package lab11fx;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author randy
 */
public class AdministradorArchivos {
    
    
    public static ArrayList<Producto> getArchivo(){
        
        ArrayList<Producto> productos;
        productos = new ArrayList<>();
        
        String nombre = "";
        float precioSinImpuesto = 0;
        float porcentajeImpuesto = 0;
        Producto.tipoVenta tipo = Producto.tipoVenta.POR_PESO;
        try {
            FileInputStream archivo = new FileInputStream("productos.xls"); //Traer el archivo a un objeto FileInputStream
            HSSFWorkbook excel = new HSSFWorkbook(archivo); //Convierte el archivo a tipo .xls
            HSSFSheet hoja = excel.getSheetAt(0); //Se obtiene la primera hoja de cálculo
            Iterator<Row> filas = hoja.iterator();
            Iterator<Cell> celdas;
            
            
            Row fila;
            Cell celda;
            int contador_celda = 0;
            int contador_fila = 0;
            fila = filas.next();//Para saltarse la primera linea del archivo
            while(filas.hasNext()){
                fila = filas.next();
                celdas = fila.cellIterator();
                while (celdas.hasNext()){
                    celda = celdas.next();
                    

                    switch (contador_celda){
                        case 0:
                            nombre = celda.getStringCellValue();
                            break;
                        case 1:
                            precioSinImpuesto = (float) celda.getNumericCellValue();
                            break;
                        case 2:
                            porcentajeImpuesto = (float) celda.getNumericCellValue();
                            break;
                        case 3:
                            if ("PESO".equals(celda.getStringCellValue())){
                                tipo = Producto.tipoVenta.POR_PESO;
                            }
                            else {
                                tipo = Producto.tipoVenta.POR_UNIDAD;
                            }
                            break;

                    }
                    contador_celda ++;
                }
                productos.add(new Producto(nombre, precioSinImpuesto, porcentajeImpuesto, tipo));
                contador_celda = 0;
            }
            excel.close();
            archivo.close();
        }  
        catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
            //Logger.getLogger(AdministradorArchivos.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            //Logger.getLogger(AdministradorArchivos.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return productos;
        
    }
    
    public static void agregarProducto(String nombre, float precio, float porcentajeImpuesto, String tipo){
    
                FileInputStream archivo;
                int hojaFinal;

        try {
            archivo = new FileInputStream("productos.xls"); //Traer el archivo a un objeto FileInputStream
            HSSFWorkbook excel = new HSSFWorkbook(archivo); //Convierte el archivo a tipo .xls
            HSSFSheet hoja = excel.getSheetAt(0); //Se obtiene la primera hoja de cálculo
            
            hojaFinal = hoja.getLastRowNum() + 1;
            hoja.createRow(hojaFinal);
            hoja.getRow(hojaFinal).createCell(0).setCellValue(nombre);
            hoja.getRow(hojaFinal).createCell(1).setCellValue(precio);
            hoja.getRow(hojaFinal).createCell(2).setCellValue(porcentajeImpuesto);
            hoja.getRow(hojaFinal).createCell(3).setCellValue(tipo);
            archivo.close();
            FileOutputStream nE = new FileOutputStream("productos.xls");
            excel.write(nE);
            nE.close();
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
                
    
    }
}
     
