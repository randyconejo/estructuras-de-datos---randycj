/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab11fx;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author randy
 */
public class AgregaClienteController implements Initializable {
    
    @FXML
    TextField txtFNombre;
    
    @FXML
    CheckBox cbMembresía;
    
    @FXML
    Button btnAgregarCliente;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    public void continuar(){
        Lab11FX.continuarAC(txtFNombre.getText(), cbMembresía.isSelected());
        
    }
    
}
