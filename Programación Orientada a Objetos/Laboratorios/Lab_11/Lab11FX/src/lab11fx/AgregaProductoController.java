/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab11fx;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author randy
 */
public class AgregaProductoController implements Initializable {
    
    @FXML
    TextField txtFNombre;
    
    @FXML
    TextField txtFPrecio;
    
    @FXML
    TextField txtFImpuesto;
    
    @FXML
    ChoiceBox<String> cbTipoVenta;
    
    @FXML
    Button btnAgregarProducto;
    
    ObservableList<String> opciones;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        opciones = FXCollections.observableArrayList("POR PESO", "POR UNIDAD");
        cbTipoVenta.setItems(opciones);
// TODO
    }    
    
    public void continuarP(){
        Lab11FX.continuarAP(txtFNombre.getText(), (float) Integer.parseInt(txtFPrecio.getText()),(float) ((Integer.parseInt(txtFImpuesto.getText())) / 100.0), cbTipoVenta.getSelectionModel().getSelectedItem());
    }
}
