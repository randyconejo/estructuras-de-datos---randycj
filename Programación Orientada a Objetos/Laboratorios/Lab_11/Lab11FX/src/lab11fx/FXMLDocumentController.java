/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab11fx;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

/**
 *
 * @author randy
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    Button btnAgregarCliente;
    
    @FXML
    Button btnAgregarProducto;
    
    @FXML
    Button btnVerClientes;
    
    @FXML
    Button btnVerProductos;

    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    public void agregaCliente(){
        Lab11FX.agregarCliente();
    }
    
    public void agregaProducto(){
        Lab11FX.agregarProducto();
    }
    
    public void verClientes(){
        Lab11FX.verClientes();
    }
    
    public void verProductos(){
        Lab11FX.verProductos();
    }
}
