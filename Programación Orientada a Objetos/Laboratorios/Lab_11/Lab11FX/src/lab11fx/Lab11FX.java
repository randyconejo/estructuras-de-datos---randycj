/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab11fx;

import java.util.Scanner;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author randy
 */
public class Lab11FX extends Application {
    static Stage stage1;
    static Scene scMenu;
    static Scene scAgregaCliente;
    static Scene scAgregaProducto;
    Scanner entrada = new Scanner(System.in);
    static Supermercado supermercado = new Supermercado("Palí");
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent R1 = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        Parent R2 = FXMLLoader.load(getClass().getResource("AgregaCliente.fxml"));
        Parent R3 = FXMLLoader.load(getClass().getResource("AgregaProducto.fxml"));
        stage1 = new Stage();
        scMenu = new Scene(R1);
        scAgregaCliente = new Scene(R2);
        scAgregaProducto = new Scene(R3);

        
        stage1.setTitle("Supermercado");
        stage1.setScene(scMenu);
        stage1.show();
        
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
    public static void agregarCliente(){
        stage1.setScene(scAgregaCliente);
        stage1.show();
    }

    public static void agregarProducto(){
        stage1.setScene(scAgregaProducto);
        stage1.show();
    }
    
    public static void continuarAC(String nombre, boolean membresia){
        if (membresia == true){
            supermercado.agregarCliente(nombre, new Membresia("Membresía Básica", (float) 0.05));
            supermercado.imprimirClientes();
        }
        else {
            supermercado.agregarCliente(nombre);
            supermercado.imprimirClientes();
        }
        stage1.setScene(scMenu);
        stage1.show();
    }
    
    public static void continuarAP(String nombre, float precio, float impuesto, String tipo){
        if ("POR PESO".equals(tipo)){
            supermercado.agregarProductoInventarioVentaPorPeso(nombre, precio, impuesto);
            AdministradorArchivos.agregarProducto(nombre, precio, impuesto, "PESO");
        }
        else {
            supermercado.agregarProductoInventarioVentaUnitaria(nombre, precio, impuesto);
            AdministradorArchivos.agregarProducto(nombre, precio, impuesto, "UNIDAD");
        }
        
        supermercado.imprimirInventario();
        stage1.setScene(scMenu);
        stage1.show();
        
    }
    
    public static void verClientes(){
        supermercado.imprimirClientes();
    }
    
    public static void verProductos(){
        supermercado.imprimirInventario();
    }
    

}
