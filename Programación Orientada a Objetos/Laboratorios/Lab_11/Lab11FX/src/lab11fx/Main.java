package lab11fx;

import java.util.ArrayList;
import java.util.Scanner;
/**********************************************************************
 Instituto Tecnológico de Costa Rica
 Estructuras de Datos IC-2001
 II Semestre 2018
 Profesora: Samanta Ramijan Carmiol

 Ejemplos Prácticos: Aplicacion Completa
 **********************************************************************/
public class Main {
    public static void principal() {
        
        Scanner entrada = new Scanner(System.in);
        Supermercado supermercado = new Supermercado("Palí");
        int eleccion;
        String nombre;
        float precio;
        float impuesto;
        do {
            System.out.println("Configuración del Supermercado");
            System.out.println("(1) Agregar Cliente");
            System.out.println("(2) Agregar Producto");
            System.out.print("Ingrese su selección: ");
            eleccion = entrada.nextInt();
            
            switch (eleccion){
            
                case 1:
                    System.out.println("¿Desea agregar al cliente con una membresía?");
                    System.out.println("1. Si | 0: No");
                    eleccion = entrada.nextInt();
                    if (eleccion == 1){
                        System.out.print("Ingrese el nombre del cliente: ");
                        nombre = entrada.nextLine();
                        nombre = entrada.nextLine();//Posible error, duplicar esta linea
                        supermercado.agregarCliente(nombre, new Membresia("Membresía Básica", (float) 0.05));
                    }
                    else {
                        System.out.print("Ingrese el nombre del cliente: ");
                        nombre = entrada.nextLine();
                        nombre = entrada.nextLine();//Posible error, duplicar esta linea
                        supermercado.agregarCliente(nombre);
                    }
                    eleccion = 1;
                    System.out.println("Cliente agregado exitosamente");
                    break;
                
                case 2:
                    System.out.print("Ingrese el nombre del producto: ");
                    nombre = entrada.nextLine();
                    nombre = entrada.nextLine();//posible error
                    System.out.print("Ingrese el precio del producto sin impuestos: ");
                    precio = entrada.nextFloat();
                    System.out.print("Ingrese el porcentaje del impuesto al producto (en decimales): ");
                    impuesto = entrada.nextFloat();
                    System.out.println("¿El precio del producto es por peso o por unidad?");
                    System.out.println("1. Peso | 2: Unidad");
                    eleccion = entrada.nextInt();
                    
                    if (eleccion == 1){
                        supermercado.agregarProductoInventarioVentaPorPeso(nombre, precio, impuesto);
                        AdministradorArchivos.agregarProducto(nombre, precio, impuesto, "PESO");
                    }
                    else {
                        supermercado.agregarProductoInventarioVentaUnitaria(nombre, precio, impuesto);
                        AdministradorArchivos.agregarProducto(nombre, precio, impuesto, "UNIDAD");
                    }
                    System.out.println("Producto agregado exitosamente");
                    ;
            }
            
        
        }while(eleccion != 0);
        supermercado.imprimirClientes();
        supermercado.imprimirInventario();
        supermercado.imprimirMembresias();
    
    }
}
