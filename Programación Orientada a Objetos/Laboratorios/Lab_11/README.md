**Laboratorio 11**

1. Importe en el proyecto las siguientes clases, vistas en el laboratorio 7.
    
    Carrito.java
    
    Cliente.java
    
    Factura.java
    
    Membresia.java
    
    Producto.java
    
    Promocion.java
    
2. Cree un archivo productos.xls que contenga la información de al menos 10 productos, con el siguiente formato.

Nombre          Precio sin impuesto         Porcentaje Impuesto         Tipo de Venta

Coca-Cola 2lt        1300                           0.13                    UNIDAD

Tomate                700                           0.00                     PESO


3. Cree una clase AdministradorArchivos.java que contenga un método que permita cargar el archivo de productos en la memoria del programa. 
Recuerde manejar las posibles excepciones

4. Cree su propia clase Main.java y utilice la clase AdministradorArchivos.java para popular los productos y solicite al usuario las información restante de promociones y membresías para configurar el supermercado.

Para este laboratorio fueron necesarias implementar dos bibliotecas, las cuales están en la carpeta.
