package pruebafx;

import java.util.Date;

public class Cuenta{
  
  // Declaración de los atributos
  private int id;
  private double balance;
  private double tasaDeInteresAnual;
  private Date fechaDeCreacion = new Date();

  // Constructor por defecto sin atributos
  public Cuenta(){
    this.id = 0;
    this.balance = 0;
    this.tasaDeInteresAnual = 0;

  }

  // Constructor con atributos
  public Cuenta(int id, double balance){
    this.id = id;
    this.balance = balance;
  }

  public int getid(){
    return this.id;
  }

  public double getbalance(){
    return this.balance;
  }

  public void setbalance(double balance){
    this.balance = balance;
  }

  public double gettasaDeInteresAnual(){
    return this.tasaDeInteresAnual;
  }

  public void settasaDeInteresAnual(double tasaDeInteresAnual){
    this.tasaDeInteresAnual = tasaDeInteresAnual;
  }

  public Date getfechaDeCreacion(){
    return this.fechaDeCreacion;
  }

  public double obtenerTasaDeInteresMensual(){
    return calcularInteresMensual();
  }

  public double calcularInteresMensual(){
    return (this.balance * this.tasaDeInteresAnual);
  }

  public void retirarDinero(double cantidad){
    this.balance = this.balance - cantidad;
  }

  public void depositarDinero(double cantidad){
    this.balance = this.balance + cantidad;
  }
}

