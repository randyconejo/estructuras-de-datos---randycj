/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebafx;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author randy
 */
public class PruebaFX extends Application {
    
    private int ID;
    private int monto;
    
    @Override
    public void start(Stage primaryStage) {
        Cuenta[] arregloCuentas = new Cuenta[10];
        for(int i=0; i<10; i++){
            arregloCuentas[i] = new Cuenta(i, 100000);
        }
        
        
       // Panel para pedir ID
        Stage stage = new Stage();
        Label lblID = new Label("Ingrese su ID");
        TextField txtFID = new TextField();         
        HBox panel1 = new HBox();
        panel1.setAlignment(Pos.CENTER);
        panel1.setSpacing(10);

        panel1.getChildren().addAll(lblID, txtFID);
        Scene sceneID = new Scene(panel1, 350, 100);
        stage.setTitle("ATM");
        
        //Panel para desplegar el menú
        Button btnBalance = new Button("Ver balance actual");
        Button btnRetirar = new Button("Retirar dinero");
        Button btnDepositar = new Button("Depositar dinero");
        Button btnSalir = new Button("Salir");
        
        VBox panel2 = new VBox();
        panel2.setAlignment(Pos.CENTER);
        panel2.setSpacing(15);
        
        panel2.getChildren().addAll(btnBalance, btnRetirar, btnDepositar, btnSalir);
        Scene sceneMenu = new Scene(panel2, 250, 200);
        
        //Panel para la opción de retirar dinero
        Label lblRetirar = new Label("Ingrese la cantidad que desea retirar");
        TextField txtFRetirar = new TextField();
        GridPane panel4 = new GridPane();
        
        panel4.setAlignment(Pos.CENTER);
        panel4.setPadding(new Insets(11.5, 12.5, 13.5, 14.5));
        panel4.setHgap(5.5);
        panel4.setVgap(5.5);
        
        panel4.add(lblRetirar, 1, 0);
        panel4.add(txtFRetirar, 1, 1);
        Scene sceneRetirar = new Scene(panel4, 400, 250);
        
        //Panel para la opción de depositar dinero
        Label lblDepositar = new Label("Ingrese la cantidad que desea depositar");
        TextField txtFDepositar = new TextField();
        GridPane panel5 = new GridPane();
        
        panel5.setAlignment(Pos.CENTER);
        panel5.setPadding(new Insets(11.5, 12.5, 13.5, 14.5));
        panel5.setHgap(5.5);
        panel5.setVgap(5.5);
        
        panel5.add(lblDepositar, 1, 0);
        panel5.add(txtFDepositar, 1, 1);
        Scene sceneDepositar = new Scene(panel5, 400, 250);
    
    
        //Configuración de TextFiel de ID
        txtFID.setOnAction(new EventHandler<ActionEvent>() {

                @Override
                public void handle(ActionEvent event) {
                    ID = Integer.parseInt(txtFID.getText());
                    if (ID > 10  || ID < 0){//ID tiene que ser numero
                        System.out.println("El ID ingresado no existe");
                    }
                    else{
                        stage.setScene(sceneMenu);
                        stage.show();
                    }
                }
        });
        
        //Configuración del boton para ver balance
        btnBalance.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                System.out.print("\nSu balance es de: " + arregloCuentas[ID].getbalance() + " colones");
            }
        });
        
        //Configuración del boton para retirar dinero
        btnRetirar.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                stage.setScene(sceneRetirar);
                stage.show();
            }
        });
        
        //Configuración del boton para depositar dinero
        btnDepositar.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                stage.setScene(sceneDepositar);
                stage.show();
            }
        });
        
        //Configuración del boton para salir del menú
        btnSalir.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                stage.setScene(sceneID);
                stage.show();
            }
        });
        
        //Configuración para el TextFiel de Retirar
        txtFRetirar.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                monto = Integer.parseInt(txtFRetirar.getText());
                arregloCuentas[ID].retirarDinero(monto);
                System.out.println("\nSe retiraron " + monto + " colones");
                stage.setScene(sceneMenu);
                stage.show();
            }
        });
        
        //Configuración para el TextFiel de Depositar
        txtFDepositar.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event){
                monto = Integer.parseInt(txtFDepositar.getText());
                arregloCuentas[ID].depositarDinero(monto);
                System.out.println("\nSe depositaron " + monto + " colones");
                stage.setScene(sceneMenu);
                stage.show();
            }
        });
        

        // Se inicia el programa con el panel de pedir ID
        stage.setScene(sceneID);
        stage.show();
    
}
    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
    
}
